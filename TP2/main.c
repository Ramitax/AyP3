#include <stdio.h>

typedef struct {
    char *name_chield;
    int age;
} chield;

typedef struct {
    char *name;
    char *surname;
    int age;
    chield chields[2];
} persona;

int main(int argc, char *argv[]) {
    chield nicolas = {"Nicolas",  21};
    chield chields[2];
    chields[0] = nicolas;
    persona ramiro = {"Ramiro", "Ambrosetti",21, chields};

    printf("Name: %s\n Age: %d\n Chields: %s\n", ramiro.name, ramiro.age, ramiro.chields[0].name_chield);

    return 0;
}