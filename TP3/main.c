#include <stdio.h>
#include <stdlib.h>


typedef struct structNode {
	int value;
	struct structNode *next;
} Node;


Node *createList(){
	Node *list = malloc(sizeof(Node));
	list = NULL;
	return list;
}

int *length(Node *list){
	if(list == NULL){
		return 0;
	} else {
			Node *pointer = list;
		int value = 1;
		while(pointer->next != NULL){
			value = value + 1;
			pointer = pointer->next;
		}
		return value;
	}
}

int searchInPosition(Node *list, int position){
	Node* pointer = list;
    if (position < length(list)){
		int i;
        for (i= 0;i < position; i++){
            pointer = pointer->next;
        }
    }else{
        printf("La posicion ingresada no se encuentra en la lista");
    }
    return pointer->value;

}

Node *addElement(Node *list, int value){
	Node *newNode = malloc(sizeof(Node));
	newNode->value = value;
	newNode->next=NULL;
	
	if(list != NULL){
		Node *pointer = list;
		while(pointer->next != NULL){
			pointer = pointer->next;
		}
		pointer->next = newNode;
	}
	else{
		list=newNode;
	}
	return list;
}

Node *deleteInPosition(Node *list, int position) {
	Node *pointer = list;
	Node *newList = createList();
	int currentPosition = 0;
	while(pointer->next != NULL){
		if(currentPosition != position){
			int value = pointer->value;
			newList = addElement(newList, value);
		}
		currentPosition = currentPosition + 1;
		pointer = pointer->next;
	}
	if(currentPosition != position){
		newList = addElement(newList, pointer->value);
	}
	return newList;
}

void print(Node *list){
	Node *pointer = list;
	int i;
    for (i = 0; i < length(list) ; i++){
        printf("[%d] : %d \n", i,  pointer->value);
        pointer = pointer->next;
    }
}

Node *sortedList (Node *list){
    Node *pointer = list;
    int lowerValue = 0;
    int highValue = 0;
    int large = length(list);
    int y;
    for (y = 0; y < (large*2); y++){
    	int x;
        for (x = 0; x < (large-1); x++){
            if (pointer->value > pointer->next->value){
                lowerValue = pointer->next->value;
                highValue = pointer->value;
                pointer->value = lowerValue;
                pointer->next->value = highValue;
            }else{
                pointer = pointer->next;
            }
        }
        pointer = list;
    }
    return list;
}


int main (){
	Node *list = createList();
	list = addElement(list, 4);
	list = addElement(list, 7);
    list = addElement(list, 2);
    list = addElement(list, -1);
    list = addElement(list, 2);
    list = addElement(list, 9);
    list = addElement(list, 6);
    
    printf("Impresion de la lista: \n");
    print(list);
    printf("La lista tiene un largo de %d \n", length(list));
    printf("En la posicion 0: %d \n", searchInPosition(list, 0));
    printf("En la posicion 1: %d \n", searchInPosition(list, 1));
    printf("En la posicion 2: %d \n", searchInPosition(list, 2));
    printf("En la posicion 3: %d \n", searchInPosition(list, 3));
    printf("En la posicion 4: %d \n", searchInPosition(list, 4));
    printf("En la posicion 5: %d \n", searchInPosition(list, 5));
    printf("En la posicion 6: %d \n", searchInPosition(list, 6));
    list = sortedList(list);
    printf("Impresion de la lista ordenada: \n");
    print(list);
    list = deleteInPosition(list, 4);
    printf("Se elimino el elemento en la posicion 4 \n");
    printf("Impresion de la lista: \n");
    print(list);
    
	return 0;
}

