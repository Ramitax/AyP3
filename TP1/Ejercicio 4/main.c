#include <stdio.h>

int main() {
    printf("Bienvenido al Ejercicio 4: Promedio\n");
    int num;
    int num_aux;
    printf("Escribe los numeros:\n");
    for (int i = 0; i < 3; ++i) {
        printf("%d: ", i+1);
        scanf("%d", &num);
        num_aux += num;
    }
    float promedio = (float)num_aux/3;
    printf("El promedio es %f", promedio);
    return 0;
}
