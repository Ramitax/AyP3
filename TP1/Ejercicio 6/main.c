#include <stdio.h>

int main() {
    printf("Bienvenido al Ejercicio 6: Número par\n");
    int numero;
    printf("Ingrese un numero: ");
    scanf("%d", &numero);
    if ((numero % 2) == 0){
        printf("El numero es par");
    } else {
        printf("El numero es impar");
    }
}
