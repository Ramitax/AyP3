#include <stdio.h>
#include<stdbool.h>

int main() {
    int opcion;
    printf("Bienvenido al Ejercicio 7: Menú\n");
    printf("1. Opcion 1\n2. Opcion 2\n3. Opcion 3\n4. Salir\n");
    while(true){
        scanf("%d", &opcion);
        if (opcion == 1){
            printf("¿Cuál es el colmo de un ciego?\nLlamarse Casimiro, vivir en el noveno B, de la calle buena vista y vender tuberías.\n");
        }
        if (opcion == 2){
            printf("¿Cuál es el colmo de un astronauta?\nEnfermarse de gravedad.\n");
        }
        if (opcion == 3){
            printf("¿Cuál es el colmo de un sordo?\nQue al morir le dediquen un minuto de silencio.\n");
        }
        if (opcion == 4){
            printf("Saliendo...");
            return 0;
        }
    }
}
