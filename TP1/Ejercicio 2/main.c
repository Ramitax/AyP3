#include <stdio.h>

int main() {
    int tamanio;
    printf("Bienvenido al Ejercicio 2: Máximo\n");
    printf("Que tamaño quiere que tenga su Array\n");
    scanf("%d", &tamanio);
    int numeros[tamanio];
    int num, mayor = -9999;
    for (int i = 0; i < tamanio; i++) {
        printf("%d:", i);
        scanf("%d", &num);
        if (mayor < num){
            mayor = num;
        }
        numeros[i] = num;
    }
    printf("Array: ");
    for (int i = 0; i < tamanio; i++) {
        if (i == (tamanio - 1)){
            printf("%d.", numeros[i]);
        }else{
            printf("%d, ", numeros[i]);
        }
    }
    printf("\nEl numero mas grande es el: %d", mayor);
    return 0;
}
