#include <stdio.h>

int main() {
    printf("Bienvenido al Ejercicio 5: ASCII\n");
    printf("ASCII: ");
    for (int i = 0; i <= 255; ++i) {
        if (i == 255){
            printf("%c.", i);
        } else{
            printf("%c, ", i);
        }
    }
}
