#include <stdio.h>

int main() {
    int tamanio;
    printf("Bienvenido al Ejercicio 3: Minimo\n");
    printf("Que tamaño quiere que tenga su Array\n");
    scanf("%d", &tamanio);
    int numeros[tamanio];
    int num, manor = 9999;
    for (int i = 0; i < tamanio; i++) {
        printf("%d:", i);
        scanf("%d", &num);
        if (manor > num){
            manor = num;
        }
        numeros[i] = num;
    }
    printf("Array: ");
    for (int i = 0; i < tamanio; i++) {
        if (i == (tamanio - 1)){
            printf("%d.", numeros[i]);
        }else{
            printf("%d, ", numeros[i]);
        }
    }
    printf("\nEl numero mas chico es el: %d", manor);
    return 0;
}
